#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>

#include "src/libraries/PubSubClient/PubSubClient.h"
#include "src/libraries/TaskScheduler/TaskScheduler.h"

#define LED_PIN 2
#define USER_RESET_PIN GPIO5

#define MAX_RR_VALUE 80
#define MIN_RR_VALUE 0
#define MAX_SP02_VALUE 100
#define MIN_SP02_VALUE 50
#define MAX_TEMP_VALUE 4200
#define MIN_TEMP_VALUE 3200

#define WIFI_CONNECT_TIMEOUT 10  // in second
#define MQTT_CONNECT_TIMEOUT 10  // in second

#define SEND_DATA_INTERVAL 2

#define RESET_USER_URL "http://202.148.1.57:8501/api/v1/device/reset"
#define REMOTE_SERVER_HOST "202.148.1.57"
#define REMOTE_SERVER_PORT 8501

#define DEVICE_NAME "respinos22"
#define DEVICE_TOKEN "87654321"

/******************* GLOBAL VARIABLE *********************/

const char *ssidAP = DEVICE_NAME;
const char *passwordAP = DEVICE_TOKEN;

// WiFi parameters
char ssid[64];
char password[64];
bool networkTimeout = false;
bool networkConnected = false;
bool mqttTimeout = false;
bool mqttConnected = false;

// Measurement parameters
uint8_t respiratoryRate = 0;
uint8_t spO2 = 0;
float bodyTemperature = 0;

StaticJsonDocument<128> doc;
IPAddress myIP(0, 0, 0, 0);

// State enum
enum RespinosStateEnum {
  RESPINOS_STATE_ERROR = -1,
  RESPINOS_STATE_DISCONNECTED,
  RESPINOS_STATE_READY,
  RESPINOS_STATE_RUNNING
};

RespinosStateEnum systemState = RESPINOS_STATE_DISCONNECTED;

/*********************************************************/

/************************ MQTT ***************************/

// Broker parameters
char *mqtt_broker = "202.148.1.57";
char *mqtt_username = "respinoscl";
char *mqtt_password = "X5l4SsKJkXFy253s3X2ISJvqeU8FFi";
int mqtt_port = 1889;
char *deviceId = DEVICE_NAME;

char *dataTopic = deviceId;
char *cmdTopic;
char *stateTopic;

IPAddress mqttServer(202, 148, 1, 57);

// MQTT callback, executed when receive subscribed data
void mqttCallback(char *topic, byte *payload, unsigned int length);

WiFiClient espClient;
WiFiClient cli;
HTTPClient http;
PubSubClient client(mqttServer, mqtt_port, mqttCallback, espClient);

/*********************************************************/

/******************** TASK SCHEDULER *********************/

Scheduler userScheduler;

void sendData();
// Task to send data every 2 seconds
Task taskSendData(TASK_SECOND *SEND_DATA_INTERVAL, TASK_FOREVER, &sendData);

Task taskChangeState(TASK_SECOND * 10, TASK_FOREVER, []() {
  if (systemState == RESPINOS_STATE_READY) {
    client.publish("respinos01/cmd", "{\"cmd\":\"start\",\"mode\":\"basic\"}");
  } else {
    client.publish("respinos01/cmd", "{\"cmd\":\"stop\",\"mode\":\"basic\"}");
  }

  client.publish("respinos01/cmd", "{\"cmd\":\"state\",\"mode\":\"basic\"}");
});

bool onFlag = false;
Task taskBlinkLed(TASK_SECOND, TASK_FOREVER, []() {
  if (onFlag) {
    onFlag = false;
    digitalWrite(LED_PIN, LOW);
  } else {
    onFlag = true;
    digitalWrite(LED_PIN, HIGH);
  }
});

/*********************************************************/

/***************** FUNCTION DECLARATION ******************/

void startMeasurement();
void stopMeasurement();

/*********************************************************/

/******************* SERVER DECLARATION ******************/

ESP8266WebServer server(80);  // Initialize the server on Port 80
void handleLED();
void handleRoot();
void handleSetWifi();
void handleResetUser();

/*********************************************************/

void setup(void) {
  uint8_t timeoutCount = 0;
  // Start Serial
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT);

  Serial.print("dataTopic: ");
  Serial.println(dataTopic);

  cmdTopic = (char*) malloc((strlen(DEVICE_NAME) + strlen("/cmd")) * sizeof(char));
  strcpy(cmdTopic, DEVICE_NAME);
  strcat(cmdTopic, "/cmd");
  Serial.print("cmdTopic: ");
  Serial.println(cmdTopic);

  stateTopic = (char*) malloc((strlen(DEVICE_NAME) + strlen("/state")) * sizeof(char));
  strcpy(stateTopic, DEVICE_NAME);
  strcat(stateTopic, "/state");
  Serial.print("stateTopic: ");
  Serial.println(stateTopic);

  // Try to connect to last ssid
  // Uncomment to forget ssid on start up
  // WiFi.disconnect(true);
  // WiFi.persistent(false);

  // Set wifi mode to AP & STA
  WiFi.mode(WIFI_AP_STA);

  // Define ssid & password
  WiFi.softAP(ssidAP, passwordAP);
  while (!networkTimeout && WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
    timeoutCount++;
    if (timeoutCount >= WIFI_CONNECT_TIMEOUT) {
      networkTimeout = true;
      Serial.println("\nWifi Timeout");
    }
  }

  // Add tasks to the scheduler
  userScheduler.addTask(taskSendData);
  userScheduler.addTask(taskChangeState);
  userScheduler.addTask(taskBlinkLed);
  userScheduler.startNow();

  // Set webserver

  server.on("/", handleRoot);
  server.on("/LED", HTTP_POST, handleLED);
  server.on("/set-wifi", HTTP_POST, handleSetWifi);
  server.on("/reset-user", HTTP_POST, handleResetUser);
  server.onNotFound(handleNotFound);
  server.begin();  // Start the HTTP Server

  IPAddress HTTPS_ServerIP = WiFi.softAPIP();
  Serial.print("Server IP is: ");
  Serial.println(HTTPS_ServerIP);

  // Set system state
  if (!networkTimeout) {  // Connected to the network
    networkConnected = true;
    timeoutCount = 0;

    Serial.println("");
    Serial.println("WiFi connected");

    client.setServer(mqtt_broker, mqtt_port);
    while (!client.connected() && !mqttTimeout) {
      String client_id = "respinos-";
      client_id += String(WiFi.macAddress());
      Serial.printf("The client %s connects to the xirka iot mqtt broker\n",
                    client_id.c_str());
      if (client.connect(client_id.c_str(), mqtt_username, mqtt_password)) {
        Serial.println("xirka iot mqtt broker connected");
      } else {
        Serial.print("failed with state ");
        Serial.print(client.state());
        delay(2000);
      }

      timeoutCount++;
      if (timeoutCount >= MQTT_CONNECT_TIMEOUT) {
        mqttTimeout = true;
      }
    }

    if (!mqttTimeout) {
      mqttConnected = true;
      Serial.print("topic subs: ");
      Serial.println(cmdTopic);
      client.subscribe(cmdTopic);
      systemState = RESPINOS_STATE_READY;
    }
  } else {
    Serial.println("RESPINOS_STATE_DISCONNECTED");
    systemState = RESPINOS_STATE_DISCONNECTED;
  }
}

void loop() {
  switch (systemState) {
    case RESPINOS_STATE_DISCONNECTED:
      server.handleClient();
      break;
    default: {
      userScheduler.execute();
      client.loop();
      server.handleClient();
    }
  }
}

void handleRoot() {
  doc.clear();

  doc["network"] = networkConnected ? "Connected" : "Not Connected";
  doc["mqtt"] = mqttConnected ? "Connected" : "Not Connected";

  String msg;
  serializeJson(doc, msg);
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "text/json", msg.c_str());
}

void handleSetWifi() {
  networkTimeout = false;
  Serial.println("handleSetWifi");
  uint8_t timeoutCount = 0;

  DeserializationError error = deserializeJson(doc, server.arg("plain"));
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    server.sendHeader("Access-Control-Allow-Origin", "*");
    server.send(400, "text/json", error.f_str());
    return;
  }

  const char *ss = doc["ssid"];
  const char *pw = doc["password"];

  doc.clear();
  doc["message"] = "ok";

  String msg;
  serializeJson(doc, msg);
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "text/json", msg.c_str());

  Serial.print("ssid: ");
  Serial.println(ss);
  Serial.print("passwd: ");
  Serial.println(pw);

  // Connect to WiFi
  WiFi.begin(ss, pw);
  if (WiFi.waitForConnectResult(WIFI_CONNECT_TIMEOUT * 1000) != WL_CONNECTED) {
    Serial.println("Connection Timeout");
    Serial.print("status: ");
    Serial.println(WiFi.status());
    networkConnected = false;
    WiFi.reconnect();
  } else {
    networkConnected = true;

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.print("SSID: ");
    Serial.println(WiFi.SSID());

    // Print the IP address
    Serial.print("Local IP: ");
    Serial.println(WiFi.localIP());

    // Connect to MQTT server
    timeoutCount = 0;
    mqttTimeout = false;

    Serial.println("");
    Serial.println("WiFi connected");

    client.setServer(mqtt_broker, mqtt_port);
    while (!client.connected() && !mqttTimeout) {
      String client_id = "respinos-";
      client_id += String(WiFi.macAddress());
      Serial.printf("The client %s connects to the xirka iot mqtt broker\n",
                    client_id.c_str());
      if (client.connect(client_id.c_str(), mqtt_username, mqtt_password)) {
        Serial.println("xirka iot mqtt broker connected");
      } else {
        Serial.print("failed with state ");
        Serial.print(client.state());
        delay(2000);
      }

      timeoutCount++;
      if (timeoutCount >= MQTT_CONNECT_TIMEOUT) {
        mqttTimeout = true;
      }
    }

    if (!mqttTimeout) {
      mqttConnected = true;
      Serial.print("topic subs: ");
      Serial.println(cmdTopic);
      client.subscribe(cmdTopic);
      doc.clear();
      doc["state"] = systemState == RESPINOS_STATE_RUNNING ? "running" : "idle";

      String msg;
      serializeJson(doc, msg);

      Serial.printf("publish [%s] : %s\n", stateTopic, msg.c_str());
      client.publish(stateTopic, msg.c_str());
      systemState = RESPINOS_STATE_READY;
    }
  }
}

void handleLED() {
  digitalWrite(LED_PIN, !digitalRead(LED_PIN));
  server.sendHeader("Location", "/");
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(303);
}

void handleResetUser() {
  if (systemState == RESPINOS_STATE_RUNNING) {
    return;
  }
  doc.clear();
  doc["_id"] = ssidAP;
  doc["token"] = passwordAP;
  doc["secret"] = mqtt_password;

  String data;
  serializeJson(doc, data);
  Serial.println("Push to server");
  Serial.println(data.c_str());

  http.begin(cli, RESET_USER_URL);
  http.addHeader("Content-Type", "application/json");
  int httpResponseCode = http.POST(data.c_str());

  Serial.print("HTTP Response code: ");
  Serial.println(httpResponseCode);
  // Free resources
  http.end();

  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200);
}

void handleNotFound() {
  if (server.method() == HTTP_OPTIONS) {
    server.sendHeader("Access-Control-Allow-Origin", "*");
    server.sendHeader("Access-Control-Max-Age", "10000");
    server.sendHeader("Access-Control-Allow-Methods", "PUT,POST,GET,OPTIONS");
    server.sendHeader("Access-Control-Allow-Headers", "*");
    server.send(204);
  } else {
    server.send(404, "text/plain", "");
  }
}

void getMeasurementData() {
  respiratoryRate = random(MAX_RR_VALUE + 1);
  spO2 = random(MIN_SP02_VALUE, MAX_SP02_VALUE + 1);
  bodyTemperature = random(MIN_TEMP_VALUE, MAX_TEMP_VALUE + 1) / 100.0;
}

void sendData() {
  getMeasurementData();

  doc.clear();

  JsonObject payload = doc.createNestedObject("payload");
  payload["RR"] = respiratoryRate;
  payload["SPO2"] = spO2;
  payload["T"] = bodyTemperature;

  String msg;
  serializeJson(doc, msg);

  Serial.println(msg.c_str());

  client.publish(dataTopic, msg.c_str());
}

void mqttCallback(char *topic, byte *payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  char *s = (char *)payload;
  doc.clear();

  DeserializationError error = deserializeJson(doc, s);
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }

  const char *cmd = doc["cmd"];
  const char *mode = doc["mode"];

  Serial.print("cmd: ");
  Serial.println(cmd);
  Serial.print("mode: ");
  Serial.println(mode);

  if (!strcmp(cmd, "start")) {
    startMeasurement();
  } else if (!strcmp(cmd, "stop")) {
    stopMeasurement();
  } else if (!strcmp(cmd, "state")) {
    doc.clear();
    doc["state"] = systemState == RESPINOS_STATE_RUNNING ? "running" : "idle";

    String msg;
    serializeJson(doc, msg);

    Serial.printf("publish [%s] : %s\n", stateTopic, msg.c_str());
    client.publish(stateTopic, msg.c_str());
  }
}

void startMeasurement() {
  Serial.println("startMeasurement");
  if (systemState == RESPINOS_STATE_READY) {
    taskSendData.enable();
    systemState = RESPINOS_STATE_RUNNING;
    client.publish(stateTopic, "{\"state\":\"running\"}");
  }
}

void stopMeasurement() {
  Serial.println("stopMeasurement");
  if (systemState == RESPINOS_STATE_RUNNING) {
    taskSendData.disable();
    systemState = RESPINOS_STATE_READY;
    client.publish(stateTopic, "{\"state\":\"idle\"}");
  }
}
